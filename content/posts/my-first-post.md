---
title: "When to use @extend; when to use a mixin"
date: 2019-01-28T15:11:53-05:00
authors:
  - Cupcake Lover
tags:
  - cake
  - chocolate
image: images/test3.jpg
---

Config Dir: You can now split your configuration sections into directories per environment. Hugo did support multiple configuration files before this release, but it was hard to manage for bigger sites, especially those with multiple languages. With this we have also formalized the concept of an environment; the defaults are production (when running hugo) or development (when running hugo server) but you can create any environment you like. We will update the documentation, but all the details are in this issue. Also, see this PR for how the refactored configuration for the Hugo website looks like.

Unmarshal JSON, TOML, YAML or CSV: transform.Unmarshal (see the documentation is a new and powerful template function that can turn Resource objects or strings with JSON, TOML, YAML or CSV into maps/arrays.

Global site and hugo var: Two new global variables in site and hugo. hugo gives you version info etc. ({{ hugo.Version }}, {{ hugo.Environment }}), but the site is probably more useful, as it allows you to access the current site’s variables (e.g. {{ site.RegularPages }}) without any context (or “.”).

This version is also the fastest to date. A site building benchmark shows around 10% faster, but that depends on the site. The important part here is that we’re not getting slower. It’s quite a challenge to consistently add significant new functionality and simultaneously improve performance. It’s like not gaining weight during Christmas. We also had a small performance boost in version 0.50. A user then reported that his big and complicated site had a 30% reduction in build time. This is important to us, one of the core features. It’s in the slogan: “The world’s fastest framework for building websites.”

Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old. Richard McClintock, a Latin professor at Hampden-Sydney College in Virginia, looked up one of the more obscure Latin words, consectetur, from a Lorem Ipsum passage, and going through the cites of the word in classical literature, discovered the undoubtable source. Lorem Ipsum comes from sections 1.10.32 and 1.10.33 of "de Finibus Bonorum et Malorum" (The Extremes of Good and Evil) by Cicero, written in 45 BC. This book is a treatise on the theory of ethics, very popular during the Renaissance. The first line of Lorem Ipsum, "Lorem ipsum dolor sit amet..", comes from a line in section 1.10.32.

The standard chunk of Lorem Ipsum used since the 1500s is reproduced below for those interested. Sections 1.10.32 and 1.10.33 from "de Finibus Bonorum et Malorum" by Cicero are also reproduced in their exact original form, accompanied by English versions from the 1914 translation by H. Rackham.

        {{< highlight go "linenos=table,hl_lines=8 15-19,linenostart=1" >}}
        <!-- image -->
            <figure {{ if isset .Params "class" }}class="{{ index .Params "class" }}"{{ end }}>
                {{ if isset .Params "link"}}<a href="{{ index .Params "link"}}">{{ end }}
                    <img src="{{ index .Params "src" }}" {{ if or (isset .Params "alt") (isset .Params "caption") }}alt="{{ if isset .Params "alt"}}{{ index .Params "alt"}}{{else}}{{ index .Params "caption" }}{{ end }}"{{ end }} />
                {{ if isset .Params "link"}}</a>{{ end }}
                {{ if or (or (isset .Params "title") (isset .Params "caption")) (isset .Params "attr")}}
                <figcaption>{{ if isset .Params "title" }}
                    <h4>{{ index .Params "title" }}</h4>{{ end }}
                    {{ if or (isset .Params "caption") (isset .Params "attr")}}<p>
                    {{ index .Params "caption" }}
                    {{ if isset .Params "attrlink"}}<a href="{{ index .Params "attrlink"}}"> {{ end }}
                        {{ index .Params "attr" }}
                    {{ if isset .Params "attrlink"}}</a> {{ end }}
                    </p> {{ end }}
                </figcaption>
                {{ end }}
            </figure>
            <!-- image -->
        {{< / highlight >}}

this is a shortcode image

{{< figure class="full center" src="/images/test2.jpeg" caption= "caption by" attr= "john doe" attrlink="https://www.protechig.com"  title="from Protechig" link="/images/test2.jpeg" >}}

Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old. Richard McClintock, a Latin professor at Hampden-Sydney College in Virginia, looked up one of the more obscure Latin words, consectetur, from a Lorem Ipsum passage, and going through the cites of the word in classical literature, discovered the undoubtable source. Lorem Ipsum comes from sections 1.10.32 and 1.10.33 of "de Finibus Bonorum et Malorum" (The Extremes of Good and Evil) by Cicero, written in 45 BC. This book is a treatise on the theory of ethics, very popular during the Renaissance. The first line of Lorem Ipsum, "Lorem ipsum dolor sit amet..", comes from a line in section 1.10.32.

