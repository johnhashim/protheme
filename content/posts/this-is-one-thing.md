---
title: "this is one thing !"
date: 2019-02-4T15:11:53-05:00
image: images/test4.jpeg
---

Config Dir: You can now split your configuration sections into directories per environment. Hugo did support multiple configuration files before this release, but it was hard to manage for bigger sites, especially those with multiple languages. With this we have also formalized the concept of an environment; the defaults are production (when running hugo) or development (when running hugo server) but you can create any environment you like. We will update the documentation, but all the details are in this issue. Also, see this PR for how the refactored configuration for the Hugo website looks like.

Unmarshal JSON, TOML, YAML or CSV: transform.Unmarshal (see the documentation is a new and powerful template function that can turn Resource objects or strings with JSON, TOML, YAML or CSV into maps/arrays.

Global site and hugo var: Two new global variables in site and hugo. hugo gives you version info etc. ({{ hugo.Version }}, {{ hugo.Environment }}), but the site is probably more useful, as it allows you to access the current site’s variables (e.g. {{ site.RegularPages }}) without any context (or “.”).

This version is also the fastest to date. A site building benchmark shows around 10% faster, but that depends on the site. The important part here is that we’re not getting slower. It’s quite a challenge to consistently add significant new functionality and simultaneously improve performance. It’s like not gaining weight during Christmas. We also had a small performance boost in version 0.50. A user then reported that his big and complicated site had a 30% reduction in build time. This is important to us, one of the core features. It’s in the slogan: “The world’s fastest framework for building websites.”



